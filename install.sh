#!/bin/bash

echo "Emscripten"
echo "=========="

if [ ! -d "emsdk" ]; then
  git clone https://github.com/emscripten-core/emsdk.git
  cd emsdk
  ./emsdk install 2.0.13
  ./emsdk activate 2.0.13
  cd ..
else
  echo "emscripten already exists. delete the emsdk directory to reinstall"
fi

echo "Cloning Repos"
echo "============="

git submodule init
git submodule update
