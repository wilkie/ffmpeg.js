#!/bin/bash

echo "Emscripten Activation"
echo "====================="

source "$PWD/emsdk/emsdk_env.sh"

echo ""
echo "Emscripten Building"
echo "==================="

make all
